package com.jmavarez.bunnyjumpproject;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jmavarez.bunnyjumpproject.GameObject.Coin;

import java.util.ArrayList;

public class MyGdxGame extends ApplicationAdapter {
    ArrayList<Coin> coins = new ArrayList<Coin>();
	SpriteBatch batch;
    float stateTime;
	
	@Override
	public void create () {
		batch = new SpriteBatch();
        Assets.load();
        stateTime = 0f;
        coins.add(new Coin(Coin.GOLD, 50, 50));
        coins.add(new Coin(Coin.SILVER, 500, 150));
        coins.add(new Coin(Coin.BRONZE, 250, 250));
	}

	@Override
	public void render () {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
        batch.begin();
        stateTime += Gdx.graphics.getDeltaTime();
        renderCoin();
        batch.end();
	}

    private void renderCoin() {
        for (Coin mCoin: coins) {
            mCoin.update(stateTime);
            switch (mCoin.getType()) {
                case Coin.GOLD:
                    TextureRegion goldCoin = Assets.gold_animation.getKeyFrame(stateTime, true);
                    batch.draw(goldCoin, mCoin.position.x + ((Assets.goldCoins[0].getRegionWidth() - goldCoin.getRegionWidth()) / 2), mCoin.position.y);
                    break;
                case Coin.SILVER:
                    TextureRegion silverCoin = Assets.silver_animation.getKeyFrame(stateTime, true);
                    batch.draw(silverCoin, mCoin.position.x + ((Assets.silverCoins[0].getRegionWidth() - silverCoin.getRegionWidth()) / 2), mCoin.position.y);
                    break;
                case Coin.BRONZE:
                    TextureRegion bronzeCoin = Assets.bronze_animation.getKeyFrame(stateTime, true);
                    batch.draw(bronzeCoin, mCoin.position.x + ((Assets.bronzeCoins[0].getRegionWidth() - bronzeCoin.getRegionWidth()) / 2), mCoin.position.y);
                    break;
            }
        }
    }
	
	@Override
	public void dispose () {
		batch.dispose();
	}
}
