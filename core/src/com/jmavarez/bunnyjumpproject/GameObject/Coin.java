package com.jmavarez.bunnyjumpproject.GameObject;

public class Coin extends DynamicGameObject {
    public static final int GOLD = 1;
    public static final int SILVER = 2;
    public static final int BRONZE = 3;
    public static final int SIZE = 84;
    public static final float SPEED_ANIMATION = 0.1f;

    public float stateTime;

    private int type;

    public int getType() {
        return type;
    }

    public Coin(int type, float x, float y) {
        super(x, y, SIZE, SIZE);
        this.type = type;
        this.stateTime = 0;
    }

    public void update (float deltaTime) {
        stateTime += deltaTime;
    }
}
