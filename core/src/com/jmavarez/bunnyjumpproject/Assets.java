package com.jmavarez.bunnyjumpproject;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.jmavarez.bunnyjumpproject.GameObject.Coin;

public class Assets {
    private static TextureAtlas mAtlas;

    public static TextureRegion[] goldCoins;
    public static Animation gold_animation;
    public static TextureRegion[] silverCoins;
    public static Animation silver_animation;
    public static TextureRegion[] bronzeCoins;
    public static Animation bronze_animation;

    public static TextureRegion cactus;
    public static TextureRegion carrot;

    public static void load() {
        mAtlas = new TextureAtlas(Gdx.files.internal("spritesheet_jumper.atlas"));
        setupGoldCoin();
        setupSilverCoin();
        setupBronzeCoin();

        cactus = mAtlas.findRegion("cactus");
        carrot = mAtlas.findRegion("carrot");
    }

    private static void setupGoldCoin() {
        goldCoins = new TextureRegion[6];
        goldCoins[0] = new TextureRegion(mAtlas.findRegion("gold_1"));
        goldCoins[1] = new TextureRegion(mAtlas.findRegion("gold_2"));
        goldCoins[2] = new TextureRegion(mAtlas.findRegion("gold_3"));
        goldCoins[3] = new TextureRegion(mAtlas.findRegion("gold_4"));
        goldCoins[4] = new TextureRegion(mAtlas.findRegion("gold_3"));
        goldCoins[5] = new TextureRegion(mAtlas.findRegion("gold_2"));

        goldCoins[4].flip(true, false);
        goldCoins[5].flip(true, false);

        gold_animation = new Animation(Coin.SPEED_ANIMATION, goldCoins);
    }

    private static void setupSilverCoin() {
        silverCoins = new TextureRegion[6];
        silverCoins[0] = new TextureRegion(mAtlas.findRegion("silver_1"));
        silverCoins[1] = new TextureRegion(mAtlas.findRegion("silver_2"));
        silverCoins[2] = new TextureRegion(mAtlas.findRegion("silver_3"));
        silverCoins[3] = new TextureRegion(mAtlas.findRegion("silver_4"));
        silverCoins[4] = new TextureRegion(mAtlas.findRegion("silver_3"));
        silverCoins[5] = new TextureRegion(mAtlas.findRegion("silver_2"));

        silverCoins[4].flip(true, false);
        silverCoins[5].flip(true, false);

        silver_animation = new Animation(Coin.SPEED_ANIMATION, silverCoins);
    }

    private static void setupBronzeCoin() {
        bronzeCoins = new TextureRegion[6];
        bronzeCoins[0] = new TextureRegion(mAtlas.findRegion("bronze_1"));
        bronzeCoins[1] = new TextureRegion(mAtlas.findRegion("bronze_2"));
        bronzeCoins[2] = new TextureRegion(mAtlas.findRegion("bronze_3"));
        bronzeCoins[3] = new TextureRegion(mAtlas.findRegion("bronze_4"));
        bronzeCoins[4] = new TextureRegion(mAtlas.findRegion("bronze_3"));
        bronzeCoins[5] = new TextureRegion(mAtlas.findRegion("bronze_2"));

        bronzeCoins[4].flip(true, false);
        bronzeCoins[5].flip(true, false);

        bronze_animation = new Animation(Coin.SPEED_ANIMATION, bronzeCoins);
    }
}
